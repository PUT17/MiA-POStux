import json
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from .models import Task, Klienci, Przedmioty, Dokumentysprzedazy, Transakcje, Stanymagazynowe, Pracownicy, Typdokumentu, \
    Typpracownika, Oddzialy, Kategorie, Producenci, UserProfile
from django.http import JsonResponse
from django.contrib.auth.models import User, Group

@csrf_exempt
def get_employee_id(request):
    if request.method == "GET":
        return JsonResponse({'result': 'GET'})
    elif request.method == "POST":
        if request.content_type == 'application/json':
            cos = json.loads(request.body)
            if cos:
                if 'username' in cos:
                    username = (cos['username'])
                else:
                    username = None
        else:
            username = (request.POST.get('username', None))
        try:
            user = User.objects.get(username=username)
            employees = UserProfile.objects.get(user=user)
        except Exception as e:
            return e.message
        return JsonResponse({'idemployee': employees.idpracownika})

@csrf_exempt
def check_employee(request):
    if request.method == "GET":
        return JsonResponse({'result': 'GET'})
    elif request.method == "POST":
        if request.content_type == 'application/json':
            cos = json.loads(request.body)
            if cos:
                if 'idemployee' in cos:
                    idprac = int(cos['idemployee'])
                else:
                    idprac = -1
                if 'idshop' in cos:
                    idodd = int(cos['idshop'])
                else:
                    idodd = -1
        else:
            idprac = int(request.POST.get('idemployee', -1))
            idodd = int(request.POST.get('idshop', -1))

        if idprac <0 and idodd<0:
            employees = UserProfile.objects.all()
            employee = [e.as_dict() for e in employees]
        elif idprac < 0 and idodd >=0:
            employees = UserProfile.objects.filter(idoddzialu=idodd)
            employee = [e.as_dict() for e in employees]
        elif idprac >= 0:
            employees = UserProfile.objects.filter(idpracownika=idprac)[0]
            try:
                employee = employees.as_dict()
            except Exception as e:
                return e.message
        return JsonResponse({'employee': employee})

@csrf_exempt
def add_employee(request):
    if request.method == "GET":
        return JsonResponse({'result': 'GET'})
    elif request.method == "POST":
        idodd=None
        typeemp=None
        login=None
        hash=None
        name=None
        surname=None
        suspended=0
        fired=0

        if request.content_type == 'application/json':
            cos = json.loads(request.body)
            if cos:
                if 'idshop' in cos:
                    idodd = int(cos['idshop'])
                if 'type' in cos:
                    typeemp = (cos['type'])
                if 'login' in cos:
                    login = (cos['login'])
                if 'hash' in cos:
                    hash = (cos['hash'])
                if 'name' in cos:
                    name = (cos['name'])
                if 'surname' in cos:
                    surname = (cos['surname'])
                if 'suspended' in cos:
                    suspended = int(cos['suspended'])
                if 'fired' in cos:
                    fired = int(cos['fired'])
        else:
            idodd = int(request.POST.get('idshop'))
            typeemp = request.POST.get('type', None)
            login = request.POST.get('login', None)
            hash = request.POST.get('hash', None)
            name = request.POST.get('name', None)
            surname = request.POST.get('surname', None)
            suspended = request.POST.get('suspended', 0)
            fired = request.POST.get('fired', 0)
        try:
            idodd = Oddzialy.objects.filter(idoddzialu=idodd)[0]
            user = User.objects.create_user(username=login,
                                            first_name=name,
                                            last_name=surname,
                                            email=name+'@gmail.com',
                                            password=hash)
            group = Group.objects.get(name=typeemp)
            user.groups.add(group)
            user.save()
            employee = UserProfile.objects.create(user=user,idoddzialu= idodd,zawieszony=int(suspended),zwolniny=int(fired))
            employee.save()

        except Exception as e:
            return JsonResponse({'result': e})
        return JsonResponse({'result':True})

@csrf_exempt
def change_employee(request):
    if request.method == "GET":
        return JsonResponse({'result': 'GET'})
    elif request.method == "POST":
        if request.content_type == 'application/json':
            cos = json.loads(request.body)
            if cos:
                if 'idemployee' in cos:
                    idemp = int(cos['idemployee'])
                else:
                    idemp = None
                if 'type' in cos:
                    typeemp = (cos['type'])
                else:
                    typeemp = None
                if 'login' in cos:
                    login = (cos['login'])
                else:
                    login = None
                if 'hash' in cos:
                    hash = (cos['hash'])
                else:
                    hash= None
                if 'name' in cos:
                    name = (cos['name'])
                else:
                    name = None
                if 'surname' in cos:
                    surname = (cos['surname'])
                else:
                    surname = None
                if 'suspended' in cos:
                    suspended = int(cos['suspended'])
                else:
                    suspended = None
                if 'fired' in cos:
                    fired = int(cos['fired'])
                else:
                    fired = None
        else:
            idemp = int(request.POST.get('idemployee'))
            typeemp = request.POST.get('idtype', None)
            login = request.POST.get('login', None)
            hash = request.POST.get('hash', None)
            name = request.POST.get('name', None)
            surname = request.POST.get('surname', None)
            suspended = request.POST.get('suspended', 0)
            fired = request.POST.get('fired', 0)

        try:
            employee = UserProfile.objects.filter(idpracownika=int(idemp))[0]
            if typeemp:
                group = Group.objects.get(name=typeemp)
                employee.user.groups.clear()
                employee.user.groups.add(group)
            if login:
                employee.user.username = login
            if hash:
                employee.user.password = hash
            if name:
                employee.user.first_name = name
            if surname:
                employee.user_last_name = surname
            if suspended:
                employee.zawieszony = int(suspended)
            if fired:
                employee.zwolniny = int(fired)
            employee.save()
        except Exception as e:
            return JsonResponse({'result': e.message})
        return JsonResponse({'result': True})

@csrf_exempt
def add_order(request):
    """
    Dodawnie nowego zamowienia, POST, idclient, products  [{"productid": 1, "quantity": 4},], idemployee, iddocument
    Dodanie wpisu do Dokumentysprzedaazy, Transakcje (tyle ile produktow), Stanymagazynowe (odjecie ilosci )
    wyjscie: True jak wszystko pojdzie ok
    """
    if request.method == "GET":
        return JsonResponse({'result': 'GET'})
    elif request.method == "POST":

        if request.content_type == 'application/json':
            cos = json.loads(request.body.decode('utf-8'))
            if cos:
                if 'idclient' in cos:
                    idclient = int(cos['idclient'])
                else:
                    idclient = None
                if 'idemployee' in cos:
                    idemp = int(cos['idemployee'])
                if 'iddocument' in cos:
                    typedok = cos['iddocument']
                if 'products' in cos:
                    products = cos['products']
        else:
            idclient = (request.POST.get('idclient'))
            products = request.POST.get('products')
            idemp = (request.POST.get('idemployee'))
            typedok = (request.POST.get('iddocument'))
            products = json.loads(products)

        try:
            idshop = UserProfile.objects.filter(idpracownika=idemp)
            idshop = int((idshop[0].idoddzialu).idoddzialu)
            idemp =  UserProfile.objects.filter(idpracownika=idemp)[0]
            if idclient:
                idclient = Klienci.objects.filter(idKlienta=int(idclient))[0]
            typedok = Typdokumentu.objects.filter(idtypu=typedok)[0]

            new_dok_sprze = Dokumentysprzedazy.objects.create(idpracownika=idemp, idklienta=idclient, typdokumentu_idtypu= typedok)
            new_dok_sprze.save()

            for k in products:
                tmp_prac = Przedmioty.objects.filter(idprzedmiotu=k['idproduct'])
                new_trans = Transakcje.objects.create(iddokumentu=new_dok_sprze, idprzedmiotu=tmp_prac[0], ilosc=int(k['quantity']), cenawchwilisprzedazy=tmp_prac[0].cena)
                new_trans.save()
                tmp_stan = Stanymagazynowe.objects.filter(idprzedmiotu= k['idproduct'], idoddzialu=idshop)[0]
                tmp_stan.stan = int(tmp_stan.stan)- int(k['quantity'])
                tmp_stan.save()
        except Exception as e:
            return JsonResponse({'result':e})
        return JsonResponse({'result': True})

@csrf_exempt
def cancel_order(request):
    if request.method == "POST":
        if request.content_type == 'application/json':
            cos = json.loads(request.body)
            if cos:
                if 'iddocument' in cos:
                    id = int(cos['iddocument'])
                else:
                    id = -1
                if 'cancel' in cos:
                    cal = int(cos['cancel'])
                else:
                    cal = -1

        else:
            id = int(request.POST.get('iddocument',-1))
            cal = int(request.POST.get('cancel', -1))
        try:
            dok = Dokumentysprzedazy.objects.filter(iddokumentusprzedazy=id)[0]
            dok.anulowane = cal
            dok.save()
        except Exception as e:
            return JsonResponse({"result": e})
        return JsonResponse({"result": True})

@csrf_exempt
def check_client(request):
    """
    Zwraca wszystkie dane o kliencie w formacie JSON, POST idclient = numer_id_klienta
    Zwraca wszystkie dane o wszystkich klientach po podaniu idclient < 0 , format JSON clients = lista slownikow
    """
    if request.method == "POST":
        if request.content_type == 'application/json':
            cos = json.loads(request.body)
            if cos:
                if 'idclient' in cos:
                    id = int(cos['idclient'])
                else:
                    id = -1

        else:
            id = int(request.POST.get('idclient',-1))

        if int(id) >= 0:
            client = Klienci.objects.filter(idKlienta=id)
            return JsonResponse(client[0].as_dict())
        else:
            clients = Klienci.objects.all()
            client = [c.as_dict() for c in clients]
            return JsonResponse({"clients": client})

@csrf_exempt
def add_new_client(request):
    if request.method == 'GET':
        return JsonResponse({'name': ''})
    elif request.method == 'POST':
        if request.content_type == 'application/json':
            cos = json.loads(request.body)
            if cos:
                if 'name' in cos:
                    na = (cos['name'])
                else:
                    na= None
                if 'adres' in cos:
                    ad = (cos['adres'])
                else:
                    ad= None
                if 'nip' in cos:
                    ni = (cos['nip'])
                else:
                    ni= None
                if 'regon' in cos:
                    re = (cos['regon'])
                else:
                    re= None
                if 'email' in cos:
                    em = (cos['email'])
                else:
                    em= None
        else:
            na = request.POST.get('name')
            ad = request.POST.get('adres', None)
            ni = request.POST.get('nip', None)
            re = request.POST.get('regon', None)
            em = request.POST.get('email')

        if na != None and em != None:
            try:
                newClient = Klienci.objects.create(nazwa=na, adres=ad, nip=ni,
                                            regon=re, email=em)
                newClient.save()
            except Exception as e:
                return  JsonResponse({'result': e.message})
            return JsonResponse({'idclient': newClient.idKlienta})
        else:
            return JsonResponse({'result': False})

@csrf_exempt
def check_product_cat(request):
    if request.method == "GET":
        return JsonResponse({'desc': '' })
    elif request.method == "POST":
        if request.content_type == 'application/json':
            cos = json.loads(request.body)
            if cos:
                if 'idproduct' in cos:
                    id = cos['idproduct']
                else:
                    id = None
        else:
            id = request.POST.get('idproduct')

        desc = Przedmioty.objects.filter(idprzedmiotu=int(id))
        if desc.exists():
            return JsonResponse({'idcategory': int(desc[0].idkategorii.idkategorii), 'name':str(desc[0].idkategorii.nazwa) })
        return  JsonResponse({'desc': False})

@csrf_exempt
def check_product(request):
    """
    Zwraca wszystkie dane o produkcie w formacie JSON, POST idproduct = numer_id_produktu
    Zwraca wszystkie dane o wszystkich produktach po podaniu idproduct < 0 , format JSON products = lista slownikow
    """
    if request.method == "POST":
        if request.content_type == 'application/json':
            cos = json.loads(request.body)
            if cos:
                if 'idproduct' in cos:
                    id = cos['idproduct']
                else:
                    id = None
                if 'idshop' in cos:
                    idodd = cos['idshop']
                else:
                    idodd = None
        else:
            id = request.POST.get('idproduct', None)
            idodd = request.POST.get('idshop', None)

        if idodd:
            listOfProducts = Przedmioty.objects.all()
            dictt = [p.as_dict() for p in listOfProducts]
            for product in dictt:
                try:
                    tmp = Stanymagazynowe.objects.filter(idoddzialu=idodd, idprzedmiotu=product['idproduct'])
                    product['count'] = tmp[0].stan
                except Exception as e:
                    product['count'] = 0
            return JsonResponse({'products': dictt})
        elif id:
            client = Przedmioty.objects.filter(idprzedmiotu=id)
            return JsonResponse(client[0].as_dict())
        else:
            clients = Przedmioty.objects.all()
            client = [c.as_dict() for c in clients]
            return JsonResponse({"products": client})

@csrf_exempt
def check_product_desc(request):
    if request.method == "GET":
        return JsonResponse({'desc': '' })
    elif request.method == "POST":
        if request.content_type == 'application/json':
            cos = json.loads(request.body)
            if cos:
                if 'idproduct' in cos:
                    id = cos['idproduct']
                else:
                    id = None
        else:
            id = request.POST.get('idproduct')

        desc = Przedmioty.objects.filter(idprzedmiotu=int(id))
        if desc.exists():
            return JsonResponse({'desc': str(desc[0].opis)})
        return  JsonResponse({'desc': False})

@csrf_exempt
def update_product_stock(request):
    """ - przyjmuje: productID, sklepIP, quantitywsklepie
    bedzie mozna przez przegladarke uzupelniac zapasy w sklepe
    """
    if request.method == "POST":
        if request.content_type == 'application/json':
            cos = json.loads(request.body)
            if cos:
                if 'idshop' in cos:
                    idshop = int(cos['idshop'])
                else:
                    idshop = None
                if 'idproduct' in cos:
                    idprod = int(cos['idproduct'])
                else:
                    idprod = None
                if 'quantity' in cos:
                    quantity = int(cos['quantity'])
                else:
                    quantity = None

        else:
            idprod = int(request.POST.get('idproduct', None))
            idshop = int(request.POST.get('idshop', None))
            quantity = int(request.POST.get('quantity', 0))
        try:
            try:
                stan = Stanymagazynowe.objects.filter(idprzedmiotu=idprod, idoddzialu=idshop)[0]
                stan.stan = quantity
                stan.save()
            except Exception as a:
                shop = Oddzialy.objects.filter(idoddzialu=idshop)[0]
                product = Przedmioty.objects.filter(idprzedmiotu=idprod)[0]
                stan = Stanymagazynowe.objects.create(idoddzialu=shop, idprzedmiotu= product, stan= quantity)
                stan.save()
        except Exception as e:
            return JsonResponse({"result": e})
        return JsonResponse({"result": True})

@csrf_exempt
def update_product_info(request):
    """
    - dla panelu kierasa zeby mogl zmienic dane produktu:
    - wysylal: idproduktu, nazwe, cene, idkategorii, idproducenta, opis
    """
    if request.method == "GET":
        return JsonResponse({'result': 'GET'})
    elif request.method == "POST":
        if request.content_type == 'application/json':
            cos = json.loads(request.body)
            if cos:
                if 'idcategory' in cos:
                    idcat = int(cos['idcategory'])
                else:
                    idcat = None
                if 'idproducer' in cos:
                    idprod = int(cos['idproducer'])
                else:
                    idprod = None
                if 'idproduct' in cos:
                    idproduct = int(cos['idproduct'])
                else:
                    idproduct = None
                if 'desc' in cos:
                    desc = (cos['desc'])
                else:
                    desc = None
                if 'price' in cos:
                    price = (cos['price'])
                else:
                    price = None
                if 'name' in cos:
                    name = (cos['name'])
                else:
                    name = None
        else:
            idproduct = request.POST.get('idproduct')
            idcat = request.POST.get('idcategory', None)
            idprod = request.POST.get('idproducer', None)
            desc = request.POST.get('desc', None)
            name = request.POST.get('name', None)
            price = request.POST.get('price', None)
        try:
            product = Przedmioty.objects.filter(idprzedmiotu=idproduct)[0]
            if idcat is not None:
                product.idkategorii = Kategorie.objects.filter(idkategorii=int(idcat))[0]
            if idprod is not None:
                product.idproducenta = Producenci.objects.filter(idproducenta=int(idprod))[0]
            if desc is not None:
                product.opis = desc
            if name is not None:
                product.nazwa = name
            if price is not None:
                product.cena = price
            product.save()
        except Exception as e:
            return JsonResponse({'result': e})
        return JsonResponse({'result': True})

@csrf_exempt
def add_product(request):
    if request.method == "GET":
        return JsonResponse({'result': 'GET'})
    elif request.method == "POST":
        if request.content_type == 'application/json':
            cos = json.loads(request.body)
            if cos:
                if 'idcategory' in cos:
                    idcat = int(cos['idcategory'])
                else:
                    idcat = None
                if 'idproducer' in cos:
                    idprod = int(cos['idproducer'])
                else:
                    idprod = None
                if 'desc' in cos:
                    desc = (cos['desc'])
                else:
                    desc = None
                if 'price' in cos:
                    price = (cos['price'])
                else:
                    price = None
                if 'name' in cos:
                    name = (cos['name'])
                else:
                    name = None
        else:
            idcat = request.POST.get('idcategory', None)
            idprod = request.POST.get('idproducer', None)
            desc = request.POST.get('desc', None)
            name = request.POST.get('name', None)
            price = request.POST.get('price', None)
        try:
            cat = Kategorie.objects.filter(idkategorii=int(idcat))[0]
            prod = Producenci.objects.filter(idproducenta=int(idprod))[0]
            product = Przedmioty.objects.create(idkategorii=cat, idproducenta=prod, opis=desc, cena=price, nazwa=name)
            product.save()
        except Exception as e:
            return JsonResponse({'result': e})
        return JsonResponse({'result': True})

@csrf_exempt
def get_transaction_document(request):
    if request.method == "POST":
        if request.content_type == 'application/json':
            cos = json.loads(request.body)
            if cos:
                if 'iddocument' in cos:
                    iddocument = cos['iddocument']
                else:
                    iddocument = None
        else:
            iddocument = request.POST.get('iddocument', None)

        try:
            document = Dokumentysprzedazy.objects.get(iddokumentusprzedazy=(iddocument))
            transact_set = Transakcje.objects.filter(iddokumentu=document)
            listOfDictTransact = [t.as_dict() for t in transact_set]
        except Exception as e:
            return JsonResponse({"result": e})
        return JsonResponse({'transactions':listOfDictTransact, 'cancel': document.anulowane})

@csrf_exempt
def get_categories(request):
    if request.method == "POST":
        try:
            categories = Kategorie.objects.all()
            listOfDictCategory = [t.as_dict_id_name() for t in categories]
        except Exception as e:
            return JsonResponse({"result": e})
        return JsonResponse({'categories': listOfDictCategory})

@csrf_exempt
def get_producers(request):
    if request.method == "POST":
        try:
            producers = Producenci.objects.all()
            listOfDictProducer = [t.as_dict_id_name() for t in producers]
        except Exception as e:
            return JsonResponse({"result": e})
        return JsonResponse({'producers': listOfDictProducer})


# @api_view(['GET', 'POST'])
def task_list(request):
    """
    List all tasks, or create a new task.
    """
    if request.method == 'GET':
        # tasks = Task.objects.all()
        # serializer = TaskSerializer(tasks, many=True)
        # return Response(serializer.data)
        return JsonResponse({'foo':'bar'})

    elif request.method == 'POST':
    #     serializer = TaskSerializer(data=request.DATA)
    #     if serializer.is_valid():
    #         serializer.save()
    #         return Response(serializer.data, status=status.HTTP_201_CREATED)
    #     else:
    #         return Response(
    #             serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        pass

# @api_view(['GET', 'PUT', 'DELETE'])
def task_detail(request, pk):
    """
    Get, udpate, or delete a specific task
    # """
    # try:
    #     task = Task.objects.get(pk=pk)
    # except Task.DoesNotExist:
    #     return Response(status=status.HTTP_404_NOT_FOUND)
    #
    # if request.method == 'GET':
    #     serializer = TaskSerializer(task)
    #     return Response(serializer.data)
    #
    # elif request.method == 'PUT':
    #     serializer = TaskSerializer(task, data=request.DATA)
    #     if serializer.is_valid():
    #         serializer.save()
    #         return Response(serializer.data)
    #     else:
    #         return Response(
    #             serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    #
    # elif request.method == 'DELETE':
    #     task.delete()
    #     return Response(status=status.HTTP_204_NO_CONTENT)
    return JsonResponse({'cos':''})