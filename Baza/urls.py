from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^add_client$',  add_new_client, name='add_client'),
    url(r'^add_order$', add_order, name='add_order'),
    url(r'^check_product_desc$', check_product_desc, name='check_product_desc'),
    url(r'^check_client$', check_client, name='check_client'),
    url(r'^check_product$', check_product, name='check_product'),
    url(r'^check_employee$', check_employee, name='check_employee'),
    url(r'^change_employee$', change_employee, name='change_employee'),
    url(r'^add_employee$', add_employee, name='add_employee'),
    url(r'^check_product_cat$', check_product_cat, name='check_product_cat'),
    url(r'^update_product_stock$', update_product_stock, name='update_product_stock'),
    url(r'^update_product_info$', update_product_info, name='update_product_info'),
    url(r'^add_product$', add_product, name='add_product'),
    url(r'^cancel_order$', cancel_order, name='cancel_order'),
    url(r'^get_transaction_document$', get_transaction_document, name='get_transaction_document'),
    url(r'^get_employee_id$', get_employee_id, name='get_employee_id'),
    url(r'^get_producers$', get_producers, name='get_producers'),
    url(r'^get_categories$', get_categories, name='get_categories'),

]
