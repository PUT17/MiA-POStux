# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from Baza.models import *
from django.apps import apps

for model in apps.get_app_config('Baza').models.values():
    admin.site.register(model)