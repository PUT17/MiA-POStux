# -*- coding: utf-8 -*-
# Model zaimprotowany z SQLite, a nastepnie edytowany - ustawione odpowiednie klucze (problem podwojnych primary key etc.)
# Na podstawie tego modelu stworzyc nowa baze w Django :)
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import uuid

from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.db import models
from django.dispatch import receiver


class Braki(models.Model):
    idbraku = models.AutoField(db_column="idbraku", primary_key=True, editable=False)
    idprzedmiotu = models.ForeignKey('Przedmioty', models.DO_NOTHING, db_column='idPrzedmiotu',
                                     null=False)  # Field name made lowercase.
    idoddzialu = models.ForeignKey('Oddzialy', models.DO_NOTHING, db_column='idOddzialu',
                                   null=False)  # Field name made lowercase.
    ilosc = models.IntegerField(db_column='IloscBrakujacych', default=0)

    class Meta:
        db_table = 'Braki'
        unique_together = (('idprzedmiotu', 'idoddzialu'),)

    def __str__(self):
        return 'Numer: %s Przedmiot: %s Oddział: %s Ilosc: %s ' % (
        self.idbraku, self.idprzedmiotu, self.idoddzialu, self.ilosc)


class Zamowienia(models.Model):
    idzamowienia = models.AutoField(db_column='idZamowienia', primary_key=True)
    # idprzedmiotu = models.ForeignKey('Przedmioty', models.DO_NOTHING,
    #                                  db_column='idPrzedmiotu')  # Field name made lowercase.
    idoddzialu = models.ForeignKey('Oddzialy', models.DO_NOTHING, db_column='idOddzialu')  # Field name made lowercase.
    # ilosc = models.IntegerField(db_column='IloscZamowionych', default=0)
    wyslane = models.IntegerField(db_column='Wyslane', default=0)  # Field name made lowercase.
    data = models.DateTimeField(db_column='Data', auto_now=True)

    class Meta:
        db_table = 'Zamowienia'
        # unique_together = (('idprzedmiotu', 'idoddzialu'),)

    def __str__(self):
        return 'Numer: %s  Oddział: %s Data: %s Wyslane: %s' % (
        self.idzamowienia, self.idoddzialu, str(self.data), self.wyslane)


class ZamowieniaProdukt(models.Model):
    idzamowienia = models.ForeignKey('Zamowienia', models.DO_NOTHING, db_column='idzamowienia')
    idprzedmiotu = models.ForeignKey('Przedmioty', models.DO_NOTHING, db_column='idprzedmiotu')
    ilosc = models.IntegerField(db_column='ilosc', default=0)
    class Meta:
        db_table = 'ZamowieniaProdukt'
    def __str__(self):
        return "Zamowienie: {} Przedmiot: {}".format(self.idzamowienia, self.idprzedmiotu)
    def as_dict(self):
        obc_dict = {
            'idorder': self.idzamowienia.idzamowienia,
            'idproduct': self.idprzedmiotu.idprzedmiotu
        }
        return obc_dict

class Dokumentysprzedazy(models.Model):
    iddokumentusprzedazy = models.AutoField(db_column='idDokumentuSprzedazy', primary_key=True,
                                            null=False)  # Field name made lowercase.
    # idtransakcji = models.ForeignKey('Transakcje', models.DO_NOTHING, db_column='idTransakcji', null=False)  # Field name made lowercase.
    idpracownika = models.ForeignKey('UserProfile', models.DO_NOTHING, db_column='idPracownika',
                                     null=False)  # Field name made lowercase.
    idklienta = models.ForeignKey('Klienci', models.DO_NOTHING, db_column='idKlienta', blank=True,
                                  null=True)  # Field name made lowercase.
    typdokumentu_idtypu = models.ForeignKey('Typdokumentu', models.DO_NOTHING,
                                            db_column='TypDokumentu_idTypu')  # Field name made lowercase.
    anulowane = models.IntegerField(db_column='Anulowane', default=0)  # Field name made lowercase.
    datasprzedazy = models.DateTimeField(db_column='DataSprzedazy', auto_now=True)  # Field name made lowercase.

    class Meta:
        db_table = 'DokumentySprzedazy'

    def __str__(self):
        return 'Iddokumentusprzedazy: %s  Klient: %s typ dokumentu: %s anulowane: %s datasprzedazy: %s' % \
               (self.iddokumentusprzedazy, self.idklienta, self.typdokumentu_idtypu, self.anulowane,
                str(self.datasprzedazy))
        # return '%s' % (self.typdokumentu_idtypu)


class Kategorie(models.Model):
    idkategorii = models.AutoField(db_column='idKategorii', primary_key=True)  # Field name made lowercase.
    nazwa = models.CharField(db_column='Nazwa', max_length=45)  # Field name made lowercase.
    opis = models.TextField(db_column='Opis', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'Kategorie'

    def __str__(self):
        # return 'IdKategorii: %s Nazwa: %s Opis: %s' % (self.idkategorii, str(self.nazwa), str(self.opis))
        return '%s %s' % (self.nazwa, self.opis)

    def as_dict(self):
        obc_dict = {
            'idcategory': self.idkategorii,
            'desc': self.opis,
            'name': self.nazwa
        }
        return obc_dict

    def as_dict_id_name(self):
        ob_dict = {
            'idcategory': self.idkategorii,
            'category': self.nazwa
        }
        return ob_dict

class Klienci(models.Model):
    idKlienta = models.AutoField(db_column='idKlienta', primary_key=True)  # Field name made lowercase.
    nazwa = models.CharField(db_column='Nazwa', max_length=45)  # Field name made lowercase.
    adres = models.TextField(db_column='Adres', blank=True, null=True)  # Field name made lowercase.
    nip = models.CharField(db_column='NIP', max_length=10, blank=True, null=True)  # Field name made lowercase.
    regon = models.CharField(db_column='REGON', max_length=14, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(max_length=255)

    class Meta:
        db_table = 'Klienci'

    def __str__(self):
        # return 'IdKlienta: %s Nazwa: %s adres: %s nip: %s regon: %s email: %s' % (self.idKlienta, str(self.nazwa), str(self.adres), str(self.nip), str(self.regon), str(self.email))
        return '%s %s' % (self.nazwa, self.adres)

    def as_dict(self):
        obc_dict = {
            'idclient': self.idKlienta,
            'name': self.nazwa,
            'adres': self.adres,
            'nip': self.nip,
            'regon': self.regon,
            'email': self.email
        }
        return obc_dict


class Oddzialy(models.Model):
    idoddzialu = models.AutoField(db_column='idOddzialu', primary_key=True)  # Field name made lowercase.
    typoddzialu = models.ForeignKey('Typoddzialu', models.DO_NOTHING,
                                    db_column='TypOddzialu')  # Field name made lowercase.
    idkierownika = models.ForeignKey('UserProfile', models.DO_NOTHING, db_column='idKierownika', null=True,
                                     blank=True)  # Field name made lowercase.
    nazwa = models.CharField(db_column='Nazwa', max_length=45)  # Field name made lowercase.
    adres = models.TextField(db_column='Adres')  # Field name made lowercase. This field type is a guess.

    class Meta:
        db_table = 'Oddzialy'

    def __str__(self):
        # return 'IdOddzialu: %s TypOdzialu: %s idKierownika: %s nazwa: %s adres: %s' % (self.idoddzialu, self.typoddzialu, self.idkierownika, str(self.nazwa), str(self.adres))
        return '%s %s' % (self.nazwa, self.adres)


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    #other fields here
    idpracownika = models.AutoField(db_column='idPracownika', primary_key=True)  # Field name made lowercase.
    idoddzialu = models.ForeignKey('Oddzialy', models.DO_NOTHING, db_column='idOddzialu', null=True,
                                   blank=True)  # Field name made lowercase.
    zawieszony = models.IntegerField(default=0)
    zwolniny = models.IntegerField(default=0)

    class Meta:
        db_table = 'UserProfile'

    def __str__(self):
        # return 'IdPracownika: %s IdOddziału: %s typ pracownika: %s login: %s Imie: %s Nazwisko: %s Zawieszonu: %s zwolniony %s' % (self.idpracownika, self.idoddzialu, self.typpracownika, str(self.login), str(self.imie), str(self.nazwisko), self.zawieszony, self.zwolniny)
        return '%s %s ID:%s' % (self.user, self.idoddzialu, self.idpracownika)

    def as_dict(self):
        obc_dict = {
            'idemployee': self.idpracownika,
            'idshop': self.idoddzialu.idoddzialu,
            'type': self.user.groups,
            'login': self.user.username,
            'hash': self.user.password,
            'name': self.user.first_name,
            'surname': self.user.last_name,
            'suspended': self.zawieszony,
            'fired': self.zwolniny
        }
        return obc_dict

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
       profile, created = UserProfile.objects.get_or_create(user=instance)

post_save.connect(create_user_profile, sender=User)

class Pracownicy(models.Model):
    idpracownika = models.AutoField(db_column='idPracownika', primary_key=True)  # Field name made lowercase.
    idoddzialu = models.ForeignKey('Oddzialy', models.DO_NOTHING, db_column='idOddzialu', null=True,
                                   blank=True)  # Field name made lowercase.
    typpracownika = models.ForeignKey('Typpracownika', models.DO_NOTHING,
                                      db_column='TypPracownika')  # Field name made lowercase.
    login = models.CharField(unique=True, max_length=45, null=False)
    hash = models.CharField(max_length=45, null=False)
    imie = models.CharField(max_length=45, null=False)
    nazwisko = models.CharField(max_length=45, null=False)
    zawieszony = models.IntegerField(default=0)
    zwolniny = models.IntegerField(default=0)

    class Meta:
        db_table = 'Pracownicy'

    def __str__(self):
        # return 'IdPracownika: %s IdOddziału: %s typ pracownika: %s login: %s Imie: %s Nazwisko: %s Zawieszonu: %s zwolniony %s' % (self.idpracownika, self.idoddzialu, self.typpracownika, str(self.login), str(self.imie), str(self.nazwisko), self.zawieszony, self.zwolniny)
        return '%s %s %s' % (self.nazwisko, self.imie, self.idoddzialu)

    def as_dict(self):
        obc_dict = {
            'idemployee': self.idpracownika,
            'idshop': self.idoddzialu.idoddzialu,
            'type': self.typpracownika.nazwastanowiska,
            'login': self.login,
            'hash': self.hash,
            'name': self.imie,
            'surname': self.nazwisko,
            'suspended': self.zawieszony,
            'fired': self.zwolniny
        }
        return obc_dict

class Producenci(models.Model):
    idproducenta = models.AutoField(db_column='idProducenta', primary_key=True)  # Field name made lowercase.
    nazwa = models.CharField(db_column='Nazwa', max_length=45)  # Field name made lowercase.
    opis = models.TextField(db_column='Opis', blank=True,
                            null=True)  # Field name made lowercase. This field type is a guess.

    class Meta:
        db_table = 'Producenci'

    def __str__(self):
        # return 'IdProducenta: %s Nazwa: %s Opis: %s' % (self.idproducenta, str(self.nazwa), str(self.opis))
        return '%s %s' % (self.nazwa, self.opis)

    def as_dict_id_name(self):
        ob_dict = {
            'idproducer': self.idproducenta,
            'producer': self.nazwa
        }
        return ob_dict



class Przedmioty(models.Model):
    idprzedmiotu = models.AutoField(db_column='idPrzedmiotu', primary_key=True, editable=False)  # Field name made lowercase.
    idkategorii = models.ForeignKey('Kategorie', models.DO_NOTHING,
                                    db_column='idKategorii')  # Field name made lowercase.
    idproducenta = models.ForeignKey('Producenci', models.DO_NOTHING,
                                     db_column='idProducenta')  # Field name made lowercase.
    opis = models.TextField(db_column='Opis', blank=True,
                            null=True)  # Field name made lowercase. This field type is a guess.
    cena = models.DecimalField(db_column='Cena', max_digits=15,
                               decimal_places=2)  # Field name made lowercase. max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    nazwa = models.CharField(db_column='Nazwa', unique=True, max_length=100)  # Field name made lowercase.

    class Meta:
        db_table = 'Przedmioty'

    def __str__(self):
        # return 'IdPrzedmiotu: %s IdKategorii: %s idProducenta: %s Nazwa: %s Cena: %s Opis: %s' % (self.idprzedmiotu, self.idkategorii, self.idproducenta, str(self.nazwa), self.cena, str(self.opis))
        return '%s %s %s %s' % (self.idprzedmiotu, self.nazwa, self.cena, self.opis)

    def as_dict(self):
        obc_dict = {
            'idproduct': self.idprzedmiotu,
            'idcategory': self.idkategorii.idkategorii,
            'category': self.idkategorii.nazwa,
            'idproducer': self.idproducenta.idproducenta,
            'producer': self.idproducenta.nazwa,
            'desc': self.opis,
            'price': self.cena,
            'name': self.nazwa
        }
        return obc_dict


class Stanymagazynowe(models.Model):
    id = models.AutoField(db_column='id', primary_key=True)
    idoddzialu = models.ForeignKey('Oddzialy', models.DO_NOTHING, db_column='idOddzialu',
                                   default=0)  # Field name made lowercase.
    idprzedmiotu = models.ForeignKey('Przedmioty', models.DO_NOTHING, db_column='idPrzedmiotu',
                                     default=0)  # Field name made lowercase.
    stan = models.IntegerField()

    class Meta:
        db_table = 'StanyMagazynowe'
        # unique_together = (('przedmiotid', 'oddzialid'),)

    def __str__(self):
        return '%s %s stan: %s' % (self.idoddzialu, self.idprzedmiotu, self.stan)


class Transakcje(models.Model):
    id = models.AutoField(db_column='id', primary_key=True)
    iddokumentu = models.ForeignKey('Dokumentysprzedazy', models.DO_NOTHING, db_column='iddokumentu',
                                    default=0)  # Field name made lowercase.
    idprzedmiotu = models.ForeignKey('Przedmioty', models.DO_NOTHING, db_column='idPrzedmiotu',
                                     default=0)  # Field name made lowercase.
    ilosc = models.IntegerField(db_column='Ilosc')  # Field name made lowercase.
    cenawchwilisprzedazy = models.DecimalField(db_column='CenawChwiliSprzedazy', max_digits=10,
                                               decimal_places=5)  # Field name made lowercase. max_digits and decimal_places have been guessed, as this database handles decimal fields as float

    class Meta:
        db_table = 'Transakcje'
        # unique_together = (('idtransakcji', 'idprzedmiotu'),)

    def __str__(self):
        return 'Dokument: %s Przedmiot: %s ilosc: %s Cena w chwili sprzedazy: %s' % (
        self.iddokumentu, self.idprzedmiotu, self.ilosc, self.cenawchwilisprzedazy)

    def as_dict(self):
        obc_dict = {
            'idtransaction': self.id,
            'iddocument': self.iddokumentu.iddokumentusprzedazy,
            'product': self.idprzedmiotu.nazwa,
            'quantityt': self.ilosc,
            'price': self.cenawchwilisprzedazy
        }
        return obc_dict

class Typdokumentu(models.Model):
    idtypu = models.AutoField(db_column='idTypu', primary_key=True)  # Field name made lowercase.
    nazwa = models.CharField(db_column='Nazwa', unique=True, max_length=45)  # Field name made lowercase.

    class Meta:
        db_table = 'TypDokumentu'

    def __str__(self):
        return '%s' % (self.nazwa)


class Typoddzialu(models.Model):
    idtypu = models.AutoField(db_column='idTypu', primary_key=True)  # Field name made lowercase.
    nazwa = models.CharField(db_column='Nazwa', unique=True, max_length=45)  # Field name made lowercase.

    class Meta:
        db_table = 'TypOddzialu'

    def __str__(self):
        # return 'idTypu: %s Nazwa: %s' % (self.idtypu, str(self.nazwa))
        return '%s' % (self.nazwa)


class Typpracownika(models.Model):
    idtypu = models.AutoField(db_column='idTypu', primary_key=True)  # Field name made lowercase.
    nazwastanowiska = models.CharField(db_column='NazwaStanowiska', unique=True,
                                       max_length=45)  # Field name made lowercase.

    class Meta:
        db_table = 'TypPracownika'

    def __str__(self):
        # return 'idTypu: %s Nazwa: %s' % (self.idtypu, str(self.nazwastanowiska))
        return '%s' % (self.nazwastanowiska)


# ???????????????????????????????????????????????????????????????????????????


class Task(models.Model):
    completed = models.BooleanField(default=False)
    title = models.CharField(max_length=100)
    description = models.TextField()
