--
-- Plik wygenerowany przez SQLiteStudio v3.1.1 dnia Pt kwi 21 00:08:02 2017
--
-- U�yte kodowanie tekstu: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Tabela: Braki
DROP TABLE IF EXISTS Braki;
CREATE TABLE Braki (
  idPrzedmiotu integer NOT NULL
,  idOddzialu integer NOT NULL
,  Wyslane integer NOT NULL DEFAULT 0
,  PRIMARY KEY (idPrzedmiotu, idOddzialu)
,  FOREIGN KEY (idPrzedmiotu)
     REFERENCES Przedmioty (PrzedmiotID)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION
,  FOREIGN KEY (idOddzialu)
     REFERENCES Oddzialy (idOddzialu)
     ON DELETE NO ACTION);

-- Tabela: DokumentySprzedazy
DROP TABLE IF EXISTS DokumentySprzedazy;
CREATE TABLE DokumentySprzedazy (
  idDokumentuSprzedazy integer NOT NULL PRIMARY KEY 
,  idTransakcji integer NOT NULL
,  idPracownika integer NOT NULL
,  idKlienta integer NULL
,  TypDokumentu_idTypu integer NOT NULL
,  Anulowane integer NOT NULL DEFAULT 0
,  DataSprzedazy DATE NOT NULL
,  FOREIGN KEY (idTransakcji)
     REFERENCES Transakcje (idTransakcji)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION
,  FOREIGN KEY (idPracownika)
     REFERENCES Pracownicy (idPracownika)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION
,  FOREIGN KEY (idKlienta)
     REFERENCES Klienci (idKlienta)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION
,  FOREIGN KEY (TypDokumentu_idTypu)
     REFERENCES TypDokumentu (idTypu)
     ON DELETE NO ACTION);

-- Tabela: Kategorie
DROP TABLE IF EXISTS Kategorie;
CREATE TABLE Kategorie (idKategorii integer NOT NULL PRIMARY KEY, Nazwa VARCHAR (45) NOT NULL, Opis TEXT);

-- Tabela: Klienci
DROP TABLE IF EXISTS Klienci;
CREATE TABLE Klienci (idKlienta integer NOT NULL PRIMARY KEY, Nazwa VARCHAR (45) NOT NULL, Adres TEXT, NIP CHAR (10), REGON CHAR (14), email VARCHAR (255) NOT NULL);

-- Tabela: Oddzialy
DROP TABLE IF EXISTS Oddzialy;
CREATE TABLE Oddzialy (
   idOddzialu integer NOT NULL PRIMARY KEY 
,  TypOddzialu integer NOT NULL
,  idKierownika integer NOT NULL
,  Nazwa VARCHAR(45) NOT NULL
,  Adres MEDIUMTEXT NOT NULL
,  FOREIGN KEY (TypOddzialu)
     REFERENCES TypOddzialu (idTypu)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION
,  FOREIGN KEY (idKierownika)
     REFERENCES Pracownicy (idPracownika)
     ON DELETE NO ACTION);

-- Tabela: Pracownicy
DROP TABLE IF EXISTS Pracownicy;
CREATE TABLE Pracownicy (idPracownika integer NOT NULL PRIMARY KEY, idOddzialu integer NOT NULL, TypPracownika integer NOT NULL, login VARCHAR (45) NOT NULL UNIQUE, hash VARCHAR (45) NOT NULL, imie VARCHAR (45) NOT NULL, nazwisko VARCHAR (45) NOT NULL, zawieszony integer NOT NULL DEFAULT 0, zwolniny integer NOT NULL DEFAULT 0, FOREIGN KEY (idOddzialu) REFERENCES Oddzialy (idOddzialu) ON DELETE NO ACTION ON UPDATE CASCADE, FOREIGN KEY (TypPracownika) REFERENCES TypPracownika (idTypu) ON DELETE NO ACTION);

-- Tabela: Producenci
DROP TABLE IF EXISTS Producenci;
CREATE TABLE Producenci (
  idProducenta integer NOT NULL PRIMARY KEY 
,  Nazwa VARCHAR(45) NOT NULL
,  Opis LONGTEXT NULL);

-- Tabela: Przedmioty
DROP TABLE IF EXISTS Przedmioty;
CREATE TABLE Przedmioty (PrzedmiotID integer NOT NULL PRIMARY KEY, idKategorii integer NOT NULL, idProducenta integer NOT NULL, Opis LONGTEXT, Cena DECIMAL NOT NULL, Nazwa VARCHAR (100) NOT NULL UNIQUE, FOREIGN KEY (idKategorii) REFERENCES Kategorie (idKategorii) ON DELETE NO ACTION ON UPDATE NO ACTION, FOREIGN KEY (idProducenta) REFERENCES Producenci (idProducenta) ON DELETE NO ACTION);

-- Tabela: StanyMagazynowe
DROP TABLE IF EXISTS StanyMagazynowe;
CREATE TABLE StanyMagazynowe (
  oddzialID integer NOT NULL
,  przedmiotID integer NOT NULL
,  stan integer NOT NULL
,  PRIMARY KEY (przedmiotID, oddzialID)
,  FOREIGN KEY (oddzialID)
     REFERENCES Oddzialy (idOddzialu)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION
,  FOREIGN KEY (przedmiotID)
     REFERENCES Przedmioty (PrzedmiotID)
     ON DELETE NO ACTION);

-- Tabela: Transakcje
DROP TABLE IF EXISTS Transakcje;
CREATE TABLE Transakcje (
  idTransakcji integer NOT NULL
,  PrzedmiotID integer NOT NULL
,  Ilosc integer NOT NULL
,  CenawChwiliSprzedazy DECIMAL NOT NULL
,  PRIMARY KEY (idTransakcji, PrzedmiotID)
,  FOREIGN KEY (PrzedmiotID)
     REFERENCES Przedmioty (PrzedmiotID)
     ON DELETE NO ACTION);

-- Tabela: TypDokumentu
DROP TABLE IF EXISTS TypDokumentu;
CREATE TABLE TypDokumentu (idTypu integer NOT NULL PRIMARY KEY, Nazwa VARCHAR (45) NOT NULL UNIQUE);

-- Tabela: TypOddzialu
DROP TABLE IF EXISTS TypOddzialu;
CREATE TABLE TypOddzialu (idTypu integer NOT NULL PRIMARY KEY, Nazwa VARCHAR (45) NOT NULL UNIQUE);

-- Tabela: TypPracownika
DROP TABLE IF EXISTS TypPracownika;
CREATE TABLE TypPracownika (idTypu integer NOT NULL PRIMARY KEY, NazwaStanowiska VARCHAR (45) NOT NULL UNIQUE);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
