from django import template
from django.contrib.auth.models import Group

register = template.Library()

@register.filter(name='has_group')
def has_group(user, group_name):
    group =  Group.objects.get(name=group_name)
    return group in user.groups.all()

# @register.filter(name='get_group')
# def get_group(user):
#     group = user.groups.all()
#     # out = ''
#     # for g in group:
#     #     out+= g.name
#     return True