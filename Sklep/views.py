# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse
from django.shortcuts import render, redirect
from Baza.models import *
from django.template import loader
from django.db import models
from copy import copy
from .forms import NewClientForm
from Baza.models import *
from django.conf import settings
from django.core.urlresolvers import RegexURLResolver, RegexURLPattern



# Create your views here.
def index(request):

    return HttpResponse("Hello, world. You're at the polls index.")

def transakcje(request):
    if request.user.is_authenticated():
        listOfProducts = Przedmioty.objects.order_by('nazwa')
        listOfKlients = Klienci.objects.order_by('nazwa')
        Product_field_names = reversed(['ilosc'] + [f.name for f in Przedmioty._meta.local_fields][4:] )
        Klienci_field_names = [f.name for f in Klienci._meta.local_fields][1:]

        #dodanie do querysetu dodatkowej kolumny ilosc w zaleznosci od stanu magazynowego danego sklepu
        for product in listOfProducts:
            # todo idoddzialu zalezne od sklepu musi przychodizc z posta albo cos :D
            try:
                tmp = Stanymagazynowe.objects.filter(idoddzialu = 1, idprzedmiotu =  product.idprzedmiotu)
                product.ilosc = tmp[0].stan
            except Exception as e:
                product.ilosc = 0
        if request.method == 'POST':
            form = NewClientForm(request.POST)
            if form.is_valid():
                #todo sprawdzenie w bazie czy nei ma juz takiego klienta
                newClient = Klienci.objects.create(nazwa=form.data['name'], adres=form.data['adres'], nip=form.data['nip'], regon=form.data['regon'], email=form.data['email'])
                newClient.save()
                pass
        else:
            form = NewClientForm()
        template = loader.get_template('Sklep/transakcje.html')
        context = {
            'listOfProduct': listOfProducts,
            'Product_field_names': Product_field_names,
            'Klienci_field_names': Klienci_field_names,
            'listOfKlients': listOfKlients,
            'Product_field_names2': copy(Product_field_names),
            'form': form,
        }

        return HttpResponse(template.render(context, request))
    else:
        return redirect('login')

def stanymagazynowe(request):
    if request.user.is_authenticated():
        listOfProducts = Przedmioty.objects.order_by('nazwa')
        Product_field_names = reversed(['ilosc'] + [f.name for f in Przedmioty._meta.local_fields][4:])
        # dodanie do querysetu dodatkowej kolumny ilosc w zaleznosci od stanu magazynowego danego sklepu
        for product in listOfProducts:
            # todo idoddzialu zalezne od sklepu musi przychodizc z posta albo cos :D
            try:
                tmp = Stanymagazynowe.objects.filter(idoddzialu=1, idprzedmiotu=product.idprzedmiotu)
                product.ilosc = tmp[0].stan
            except Exception as e:
                product.ilosc = 0
        tmp = list(copy(Product_field_names))[1:]
        tmp2 = list(Product_field_names)[:1]
        Product_field_names = (tmp2 + ['IdProduktu'] + tmp)
        template = loader.get_template('Sklep/stanymagazynowe.html')
        context = {
            'listOfProduct': listOfProducts,
            'Product_field_names': Product_field_names,
        }
        return HttpResponse(template.render(context, request))
    else:
        return redirect('login')

def pracownicy(request):
    if request.user.is_authenticated():
        listOfEmployees = UserProfile.objects.order_by('user')
        Employees_field_names = ["Imie i nazwisko","Login", "Typ pracownika"]
        # Employees_field_names[:] = [''.join(x[3:6])]
        template = loader.get_template('Sklep/pracownicy.html')
        context = {
            'listOfEmployees': listOfEmployees,
            'Employees_field_names': Employees_field_names
        }
        return HttpResponse(template.render(context, request))
    else:
        return redirect('login')

def zamowienia(request):
    if request.user.is_authenticated():
        #todo idodzialu dynamiczne
        shopname = Oddzialy.objects.filter(idoddzialu=1)[0].nazwa
        listOfDocuments = Dokumentysprzedazy.objects.all()
        Document_field_names = [f.name for f in Dokumentysprzedazy._meta.local_fields]
        Document_field_names[0] = "Numer dokumentu"
        Document_field_names[1] = "Pracownik"
        Document_field_names[2] = "Klient"
        Document_field_names[3] = "Dokument"
        # Employees_field_names[:] = [''.join(x[3:6])]
        template = loader.get_template('Sklep/zamowienia.html')
        context = {
            'listOfDocuments': listOfDocuments,
            'Document_field_names': Document_field_names
        }
        return HttpResponse(template.render(context, request))
    else:
        return redirect('login')



# by ST:
def produkty(request):
    if request.user.is_authenticated():
        listOfProducts = Przedmioty.objects.order_by('nazwa')
        Product_field_names = reversed(['ilosc'] + [f.name for f in Przedmioty._meta.local_fields][4:] )


        #dodanie do querysetu dodatkowej kolumny ilosc w zaleznosci od stanu magazynowego danego sklepu
        for product in listOfProducts:
            # todo idoddzialu zalezne od sklepu musi przychodizc z posta albo cos :D
            try:
                tmp = Stanymagazynowe.objects.filter(idoddzialu = 1, idprzedmiotu =  product.idprzedmiotu)
                product.ilosc = tmp[0].stan
            except Exception as e:
                product.ilosc = 0

        template = loader.get_template('Sklep/produkty.html')
        context = {
            'listOfProduct': listOfProducts,
            'Product_field_names': Product_field_names,
            'Product_field_names2': copy(Product_field_names)
        }

        return HttpResponse(template.render(context, request))
    else:
        return redirect('login')


