# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from Baza import models
from django.db import models

# Create your models here.

class NewClient(models.Model):
    adres = models.CharField(max_length=100)
    name = models.CharField( max_length=45)
    nip = models.CharField( max_length=10, null=True, blank=True)
    regon = models.CharField( max_length=14, null=True, blank=True)
    email = models.CharField( max_length=255, null=True, blank=True)

    def __str__(self):
        return self.adres + ' ' + self.name + ' ' + self.nip + ' ' + self.regon + ' ' + self.email
