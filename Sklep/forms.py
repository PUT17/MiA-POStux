__author__ = 'Robin'
from django import forms
from .models import NewClient

class NewClientForm(forms.ModelForm):

    class Meta:
        model = NewClient
        widgets = {
            'adres': forms.TextInput(attrs={'placeholder': 'Adres...*', 'class': 'col-sm-6 no-padding'}),
            'name': forms.TextInput(attrs={'placeholder': 'Name...*', 'class': 'col-sm-6 no-padding'}),
            'nip': forms.TextInput(attrs={'placeholder': 'Nip...', 'class': 'col-sm-6 no-padding'}),
            'regon': forms.TextInput(attrs={'placeholder': 'Regon...', 'class': 'col-sm-6 no-padding'}),
            'email': forms.TextInput(attrs={'placeholder': 'email...', 'class': 'col-sm-6 no-padding'}),
        }
        fields = ('adres','name','nip','regon','email',)
        #fields_required = ['adres','name']
