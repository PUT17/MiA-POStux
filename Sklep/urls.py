__author__ = 'Robin'
from django.conf.urls import url, include
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^login/$', auth_views.login, {'template_name': 'Sklep/registration/login.html'}, name='login'),

    url(r'^transakcje$', views.transakcje, name='transakcje'),
    url(r'^stanymagazynowe$', views.stanymagazynowe, name='stanymagazynowe'),
    url(r'^pracownicy$', views.pracownicy, name='pracownicy'),
    url(r'^zamowienia$', views.zamowienia, name='zamowienia'),
    url(r'^produkty$', views.produkty, name='produkty'),
#todo    cos nowego cvgd
]