/**
 * TO-DO:
 * - funkcja ktora przelicza sume zamowienia. Wywolywana przy dodawaniu i aktualizacji
 * - porzadek w kodzie, odpowiednie nazwy id etc. (#righttable :P)
 */

$(document).ready(function(){

    var $apiaddress = "http://127.0.0.1:8000/api/";

    //
    // NAVI FIX
    //
    var SetNaviPosition = function(whereami){
        $("div.top-menu ul.nav-tabs li").each(function(){ $(this).removeClass("active") });
        $("div.top-menu ul.nav-tabs li").has("a[href='" + whereami + "']").addClass("active");
    };
    if( $(document).find('div#ptx-transaction-view').length > 0 ){
        SetNaviPosition("transakcje");
    }
    else if ($(document).find('div#ptx-stock-view').length > 0 ){
        SetNaviPosition("stanymagazynowe");
    }
    else if ($(document).find('div#ptx-order-details-view').length > 0 ){
        SetNaviPosition("zamowienia");
    }
    else if ($(document).find('div#ptx-employee-view').length > 0 ){
        SetNaviPosition("pracownicy");
    }
    else if ($(document).find('div#ptx-edit-products-view').length > 0 ){
        SetNaviPosition("produkty");
    }


    //
    // TRANSACTION VIEW
    //
    var RecalculateOrderValue = function(){
        var $sum = 0;
        $('#ptx-transaction-view #ptx-products-in-order tr.ptx-item').each( function (index) {
            //alert("123");
            var $productprice = parseFloat($(this).find('td.ptx-product-price').html());
            var $productquantity = parseInt($(this).find('td.ptx-product-quantity input').val());
            $sum += $productprice * $productquantity ;
        });
        $sum = $sum.toFixed(2);
        $('#ptx-modal-order-sum').html("Wartość zamówienia: " + $sum + "PLN");
        $('input#ptx-ordersum-value').val($sum + " PLN");
    };
    RecalculateOrderValue();
    //szukajka
    var DodajSzukajke = function(element){
        $(element).DataTable(
        {
            "bPaginate": false,
            "sDom": '<"top"f>rt<"bottom"lp><"clear">',
            "scrollY":        "200px",
            "language": {
                "search": "_INPUT_",
                "zeroRecords": "Brak odpowiadających przedmiotów",
                "searchPlaceholder": "Szukaj..."
              }
        });
    };
    DodajSzukajke($('#ptx-transaction-view #ptx-products-in-store'));
    DodajSzukajke($('#ptx-transaction-view #ptx-table-of-clients'));

    $('#clientModal').on('shown.bs.modal', function () {
       $('#ptx-transaction-view #ptx-table-of-clients').dataTable().fnAdjustColumnSizing();
    });

    // zaladowanie opisu produktu
    $("#ptx-transaction-view").on("click", "tr.ptx-item td.ptx-product-name", function () {
        var $productID = $(this).parent("tr").find("a.ptx-btn-withid").attr("id")
        var $desc;
        $.ajax({
            method: "POST",
            url: $apiaddress + "check_product_desc",
            data: { idproduct: $productID }
        })
            .done(function (response) {
               $desc = response.desc;
               $("#ptx-product-description").html($desc);
            });
        $("#ptx-product-description").html($desc);

    });
    // dodawanie nowych produktow do koszyka
    $("#ptx-transaction-view #ptx-products-in-store a.ptx-add-product-btn").click(function(){
        //alert("123");
        var par = $(this).parents('tr');
        var $quantity = par.find("td:nth(2)").html();
        var $productID = $(this).attr("id");
        if ($quantity < 1){
            alert("Brak towaru na stanie")
        }
        else if ( $('#ptx-products-in-order a#' + $productID).length != 0 ){
            // TO-DO: sprawdzic czy produkt dodawany z lewej na prawą nie jest juz po prawej stronie. Wtedy zwiekszyc tylko jego ilosc
            alert("Towar znajduje się już w zamówieniu!")
        }
        else {
            var fr = '<td class="ptx-product-quantity">' +
                '<form class="form-inline">' +
                '<input type="text" class="units" value="1" onClick="this.setSelectionRange(0, this.value.length)"> szt.</form>' +
                '</td>';
            //par.find("td:nth(2)").html($quantity-1);
            parclone = par.clone();
            parclone.find("td.ptx-product-quantity").remove(); // usun kolumne z iloscia
            $(fr).insertAfter(parclone.find("td:nth(1)"));
            $("#ptx-products-in-order").append(parclone);
            var but = $("#ptx-products-in-order:last-child").find("td:last-child").find("a:first");
            but.removeAttr("data-toggle").removeAttr("data-target").removeClass("ptx-add-product-btn").addClass("ptx-save-changes-btn").addClass("ptx-btn-withid");
            but.text('Zapisz zmianę');
            RecalculateOrderValue();
        }
    });
    // edycja produktow "w koszyku"
    $(document).on('click', "#ptx-transaction-view #ptx-products-in-order a.ptx-save-changes-btn", function() {
        var $par = $(this).parents('tr');
        var $productID = $(this).attr("id");
        var $quantity = $('.form-inline input', $par).val();
        if ($quantity < 1) {
            $par.remove();
        }
        var $quantityinstore = ($("#ptx-products-in-store a#" + $productID).parents('tr')).find('td.ptx-product-quantity').html();
        $quantityinstore = parseInt($quantityinstore);
        if ($quantity >  $quantityinstore ){
            $('td.ptx-product-quantity input', $par).val($quantityinstore);
            alert("Brak pokrycia w sklepie");
        }
        RecalculateOrderValue();
    });
    // ładowanie klienta z listy klientów do szczegółów zamówienia
    $(document).on('click', "#ptx-transaction-view table#ptx-table-of-clients a.ptx-choose-client-btn", function() {
        //alert("123");
        var $parent = $(this).parents('tr');
        var $clientname     = $parent.find('td.ptx-client-name').html();
        var $clientid       = $parent.attr("id");
        var $clientemail    = $parent.find('td.ptx-client-email').html();
        var $clientaddress  = $parent.find('td.ptx-client-address').html();
        var $clientregon    = $parent.find('td.ptx-client-regon').html();
        var $clientnip      = $parent.find('td.ptx-client-nip').html();
        $('table#ptx-client-details td.ptx-client-name').html($clientname);
        $('table#ptx-client-details td.ptx-client-email').html($clientemail);
        $('table#ptx-client-details td.ptx-client-address').html($clientaddress);
        $('table#ptx-client-details td.ptx-client-regon').html("Regon: " + $clientregon);
        $('table#ptx-client-details td.ptx-client-nip').html("NIP: " + $clientnip);
        $('a.ptx-approve-client-order-btn').attr("clientid", $clientid);
    });
    // dodawanie nowego klienta do bazy
    $("#ptx-transaction-view #ptx-new-client-form").submit(function(e) {
        e.preventDefault();
        var $name       = $('input[name="ptx-name"]').val();
        var $address    = $('input[name="ptx-address"]').val();
        var $nip        = $('input[name="ptx-nip"]').val();
        var $regon      = $('input[name="ptx-regon"]').val();
        var $email      = $('input[name="ptx-email"]').val();
        $.ajax({
          method: "POST",
          url: $apiaddress + "add_client",
          data: { name: $name, email: $email, adres: $address, nip: $nip, regon: $regon }
        })
          .done(function( msg ) {
            alert( "Data Saved" );
            $('table#ptx-client-details td.ptx-client-name').html($name);
            $('table#ptx-client-details td.ptx-client-address').html($address);
            $('table#ptx-client-details td.ptx-client-nip').html($nip);
            $('table#ptx-client-details td.ptx-client-regon').html($regon);
            $('table#ptx-client-details td.ptx-client-email').html($email);
            var newclientid = msg.idclient;
            $('a.ptx-approve-client-order-btn').attr("clientid", newclientid);
          });
    });
    // Zatwierdzenie transakcji
    var GenerateProductJSON = function(){
        var $products = [];
        $('#ptx-transaction-view #ptx-products-in-order tr.ptx-item').each( function (index) {
                var $productid = parseInt($(this).find("a.ptx-btn-withid").attr("id"));
                var $productquantity = parseInt($(this).find('td.ptx-product-quantity input').val());
                $products.push({
                    idproduct : $productid,
                    quantity : $productquantity
                });
                //$products += '"' +  productid.toString() + '" :' + productquantity.toString() + ',';
        });
        return $products;
    };
    var SendOrderData = function (idclient, idemployee, iddocument, products) {
        $.ajax({
            method: "POST",
            url: $apiaddress + "add_order",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                idclient: idclient,
                idemployee: idemployee,
                iddocument: iddocument,
                products: products
            })
        })
            .done(function () {
                alert("Transakcja dodana");
                location.reload();
        })
            .fail(function (e) {
                alert("Something goes wrong...: " + e.responseText);
            })
    };
    var SetEmployeeID = function() {

        var employeelogin = $("p#username").html();
        var employeeid = 1;
        $.ajax({
            method: "POST",
            url: $apiaddress + "get_employee_id",
            data: {
                username: employeelogin
            }
        })
            .done(function (data) {
                employeeid = parseInt(data.idemployee);
                $("p#username").attr("employeeid", employeeid);
        })
            .fail(function (e) {
                console.log("Something goes wrong...: " + e.responseText);
            });
    };
    SetEmployeeID();
    $(document).on('click', "#ptx-transaction-view a#ptx-approve-order-btn", function () {
        var idclient;
        var idemployee = parseInt($("p#username").attr("employeeid"));
        var iddocument = 1;
        var $products = GenerateProductJSON();

        //setTimeout(function() {
              SendOrderData(idclient, idemployee, iddocument, $products);
        //   },1000); // wait 800ms



    });
    // Zatwierdzenie transakcji dla konkretnego klienta
    $(document).on('click', "#ptx-transaction-view a.ptx-approve-client-order-btn", function () {
        var idclient = parseInt($(this).attr("clientid")) ;
        var idemployee = parseInt($("p#username").attr("employeeid"));
        var iddocument = 1;
        var $products = GenerateProductJSON();
        SendOrderData(idclient, idemployee, iddocument, $products);
    });
    //
    // STOCK VIEW
    //

    DodajSzukajke($('#ptx-stock-view #ptx-products-in-store'));
    // zaladuj dane szczegolowe o produkcie
    $(document).on('click', "#ptx-stock-view #ptx-products-in-store a.ptx-choose-product-btn", function () {
        var $par = $(this).parents('tr');
        var $productID = $(this).attr("id");
        var $stockquantity = parseInt($par.find(".ptx-product-quantity").html());

        $.ajax({
            method: "POST",
            url: $apiaddress + "check_product",
            data: { idproduct: $productID }
        })
            .done(function (data) {
                     $('table#ptx-item-details td.ptx-item-name').html(data.name);
                     $('table#ptx-item-details td.ptx-item-category').html("Kategoria: " + data.category);
                     $('table#ptx-item-details td.ptx-item-producer').html("Producent: " + data.producer);
            })
            .fail(function (data) {
                alert("Something goes wrong...");
            });

        $('table#ptx-item-details td.ptx-product-id').html($productID);
        $('table#ptx-item-details form.ptx-current-item-quantity input').val($stockquantity);
    });
    // zapisz nowy stan magazynowy
    $(document).on('click', "#ptx-stock-view a.ptx-save-changes-btn", function () {
        var productitd = parseInt($("#ptx-item-details td.ptx-product-id").html());
        var productquantity = parseInt($("#ptx-item-details form.ptx-current-item-quantity input").val());
        console.log(productitd + " " + productquantity);
        $.ajax({
            method: "POST",
            url: $apiaddress + "update_product_stock",
            data: {
                idshop: "1",
                idproduct: productitd,
                quantity: productquantity
            }
        })
            .done(function (response) {
               alert("Produkty zaktualizowane");
               location.reload();
            })
            .fail(function (e) {
                alert(e.response);
            });
    });
    // dodaj kilka produktow
    $(document).on('click', "#ptx-stock-view a.ptx-add-new-items-btn", function () {
        var productitd = parseInt($("#ptx-item-details td.ptx-product-id").html());
        var productquantity = parseInt($("#ptx-item-details form.ptx-current-item-quantity input").val());
        productquantity += parseInt($("#ptx-item-details td.ptx-new-item-quantity input").val());
        console.log(productitd + " " + productquantity);
        $.ajax({
            method: "POST",
            url: $apiaddress + "update_product_stock",
            data: {
                idshop: "1",
                idproduct: productitd,
                quantity: productquantity
            }
        })
            .done(function (response) {
               alert("Produkty zaktualizowane");
               location.reload();
            })
            .fail(function (e) {
                alert(e.response);
            });
    });


    //
    // PRODUCT DETAILS
    //
    DodajSzukajke($('#ptx-order-details-view #documents-in-store'));
    $(document).on("click", "#ptx-order-details-view a.ptx-view-order-details-btn", function () {
        var orderid = parseInt($(this).attr('id'));
        var table = $("#ptx-order-details-view #ptx-order-details");
        $.ajax({
            method: "POST",
            url: $apiaddress + "get_transaction_document",
            data: {
                iddocument: orderid
            }
        })
            .done(function (response) {
               //console.log(response);
               $(table).attr("orderid", "");
               $(table).find("tbody").empty();
               $(response.transactions).each(function (index, element) {
                    var name = element.product;
                    var price = element.price;
                    var quantity = element.quantityt;
                    table.append("<tr><td>" + name + "</td><td>" + price + "</td><td>"  + quantity + "</td></tr>");
               });
               if (response.cancel == 0){
                    $(table).append("<tr><td class='cell-button' colspan='3'><a id='ptx-cancel-order-btn' orderid='"+orderid+"'>Anuluj zamówienie</a></td>");
               }
               else {
                   $(table).append("<tr><td colspan='3'>Zamówienie anulowane</td></tr>")
               }
            })
            .fail(function (e) {
                alert(e.response);
            });
    });
    $(document).on("click", "#ptx-order-details-view a#ptx-cancel-order-btn", function () {
        var orderid = parseInt($(this).attr('orderid'));
        $.ajax({
            method: "POST",
            url: $apiaddress + "cancel_order",
            data: { iddocument: orderid, cancel: 1 }
        })
            .done(function (response) {
               alert("Transakcja anulowana");
               location.reload();
            })
            .fail(function (e) {
                alert(e.response);
            });
    });

    //
    //  EDIT PRODCUTS VIEW
    //
    DodajSzukajke($("#ptx-edit-products-view #ptx-products-in-store"));
    $(document).on("click", "#ptx-edit-products-view a.ptx-load-product-btn", function () {
        var itemid =  parseInt($(this).attr("itemid"));
        var form = $("#ptx-edit-products-view #ptx-product-details");
        var categories;
        var producers;
        $.ajax({
            method: "POST",
            url: $apiaddress + "get_categories"
        })
            .done(function (response) {
                    var catselect = form.find("select[name=ptx-category]");
                    catselect.html("");
                    $(response.categories).each(function (index, element) {
                        catselect.append("<option value='" + element.idcategory + "'>" + element.category + "</option>");
                     });
            })
            .fail(function (e) {
                alert(e.response);
            });
        $.ajax({
            method: "POST",
            url: $apiaddress + "get_producers"
        })
            .done(function (response) {
                var prdselect = form.find("select[name=ptx-producer]");
                prdselect.html("");
                $(response.producers).each(function (index, element) {
                        prdselect.append("<option value='" + element.idproducer + "'>" + element.producer + "</option>");
                     });
            })
            .fail(function (e) {
                alert(e.response);
            });

        $.ajax({
            method: "POST",
            url: $apiaddress + "check_product",
            data: { idproduct: itemid }
        })
            .done(function (response) {
               form.attr("productid", response.idproduct);
               form.find("input[name=ptx-productname]").val(response.name);
               form.find("input[name=ptx-price]").val(response.price);
               form.find("textarea[name=ptx-description]").val(response.desc);
               form.find("select[name=ptx-category] option[value='" +  response.idcategory + "']").attr("selected", "");
               form.find("select[name=ptx-producer] option[value='" +  response.idproducer + "']").attr("selected", "");
            })
            .fail(function (e) {
                alert(e.response);
            });
    });
    $(document).on("click", "#ptx-edit-products-view input[name=ptx-save-edited-product]", function () {
        var form = $("#ptx-edit-products-view #ptx-product-details");
        var formdata = form.serializeArray();
        var itemid = form.attr("productid");
        $.ajax({
            method: "POST",
            url: $apiaddress + "update_product_info",
            data: {
                idproduct: itemid,
                idcategory: formdata[1].value,
                idproducer: parseInt(formdata[2].value),
                desc: formdata[4].value,
                name: formdata[0].value,
                price: formdata[3].value
            }
        })
            .done(function (response) {
               alert("Done");
               //console.log(formdata);
               location.reload();
            })
            .fail(function (e) {
                alert(e.response);
            })
    });
    $(document).on("click", "#ptx-edit-products-view input[name=ptx-add-new-product]", function () {
        var form = $("#ptx-edit-products-view #ptx-product-details");
        var formdata = form.serializeArray();
        $.ajax({
            method: "POST",
            url: $apiaddress + "add_product",
            data: {
                idcategory: formdata[1].value,
                idproducer: parseInt(formdata[2].value),
                desc: formdata[4].value,
                name: formdata[0].value,
                price: formdata[3].value
            }
        })
            .done(function (response) {
               alert("Done");
               //console.log(formdata);
               location.reload();
            })
            .fail(function (e) {
                console.log(e.response);
            })
    });
});